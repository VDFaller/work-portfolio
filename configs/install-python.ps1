# -------Micromamba---------
# this assumes you have git installed, it's a dumb dependency but bzip2 for windows isn't just on people's machines I guess?
if (!(Test-Path "HKLM:\SOFTWARE\GitForWindows")) {
    Write-Error "Git for Windows is not installed. Please install it and run this script again."
    exit 1
}
$gitpath = (Get-ItemProperty -Path "HKLM:\SOFTWARE\GitForWindows").InstallPath

$prevDir = Get-Location
mkdir ~/mambainstall
New-Item -ItemType Directory -Path "~\.mamba\envs" -Force
cd ~/mambainstall
echo "Downloading micromamba to $pwd"
Invoke-Webrequest -URI https://micro.mamba.pm/api/micromamba/win-64/latest -OutFile micromamba.tar.bz2
# some people can't do bz2, so doing this via git bz2??
& "$gitpath\mingw64\bin\bzip2.exe" -d micromamba.tar.bz2
# Extract the tar file
tar xf .\micromamba.tar
Move-Item -Force Library\bin\micromamba.exe ~\.mamba\micromamba.exe
# delete mambainstall
cd $prevDir
Remove-Item -Recurse -Force ~/mambainstall

# set the profile
$profileDir = Split-Path -Path $profile -Parent
if (!(Test-Path -Path $profileDir)) {
    New-Item -ItemType Directory -Path $profileDir -Force
}
'$Env:MAMBA_ROOT_PREFIX="$HOME\.mamba\"' | Out-File $profile
"$HOME\.mamba\micromamba.exe shell hook -s powershell | Out-String | Invoke-Expression" |  Add-Content -Path $profile
"New-Alias conda Invoke-Mamba" | Add-Content -Path $profile # just a conda alias
# Actually run it
$Env:MAMBA_ROOT_PREFIX="$HOME\.mamba\"
~\.mamba\micromamba.exe shell hook -s powershell | Out-String | Invoke-Expression

# ---------scoop----------
Invoke-RestMethod -Uri https://get.scoop.sh | Invoke-Expression

# ----------poetry------------
micromamba create -n poetry python=3.11 -c conda-forge
micromamba activate poetry
scoop install pipx
pipx ensurepath
pipx install poetry
micromamba deactivate
