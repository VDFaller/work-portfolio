# install-work 
# A general work setup script for new hires and me when I need to reinstall everything.
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser

# general work
winget install -e --id Git.Git
winget install -e --id GnuPG.GnuPG # needs to sign commits
winget install -e --id Microsoft.VisualStudioCode
winget install -e --id Notepad++.Notepad++
winget install -e --id Google.Chrome
winget install -e --id Microsoft.PowerShell
winget install -e --id OpenJS.NodeJS
winget install -e --id SlackTechnologies.Slack
winget install -e --id Docker.DockerDesktop
winget install -e --id Amazon.AWSCLI
wsl --install -d Ubuntu
wsl --set-default Ubuntu

# Python
./install-python.ps1