# Run as admin
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser

./install-work.ps1
Copy-Item "$PSScriptRoot/.gitconfig" ~


# GPG stuff
# https://docs.github.com/en/authentication/managing-commit-signature-verification/generating-a-new-gpg-key
Start-Process 'C:\Program Files\Git\git-bash.exe'
# in the git bash shell
# Generate new keys
# gpg --full-generate-key
# gpg --list-secret-keys --keyid-format=long
# get the key_id where email = $ENV:git_user_email
# import existing keys
# gpg --import gpg-pub.asc
# gpg --import gpg-sc.asc
# after getting the keyid
# key_id=$(gpg --list-secret-keys --keyid-format=long | grep -E "^sec" | awk '{print $2}' | awk -F'/' '{print $2}')
# gpg --armor --export $key_id
# git config --global user.signingkey $key_id


# lumentum
winget install -e --id Box.Box

# personal 
winget install -e --id Discord.Discord
winget install -e --id Valve.Steam
winget install -e --id Google.GoogleDrive
winget install -e --id DominikReichl.KeePass
winget install -e --id OpenWhisperSystems.Signal
winget install -e --id EpicGames.EpicGamesLauncher
winget install -e --id Spotify.Spotify

