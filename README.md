# Vince Faller

## About Me
I'm a data scientist focused on DataOps and MLOps. I think DBT is doing some awesome things in the data engineering space. I've worked with JMP/JSL, Python, DBT/Databricks/SQL, Tableau, and a few other tools. I'm currently a maintainer of the [JSL VSCode Extension](https://gitlab.com/predictum/jsl-for-vscode), though that's from my last job so it's slow to update. I pretty firmly believe slowly increasing forcing functions is the best way to get people to adopt new tools and processes. 

[![linkedin](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/vincefaller/)
[![email](https://img.shields.io/badge/Email-D14836?style=for-the-badge&logo=gmail&logoColor=white)](mailto:vdfaller@gmail.com)
![phone](https://img.shields.io/badge/Phone-1--419--823--7345-008e00?style=for-the-badge&logo=phone&logoColor=white)
[![github](https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white)](https://github.com/VDFaller)
[![gitlab](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/VDFaller)

## Skills
### Languages
![DBT](https://img.shields.io/badge/DBT-FF5733?style=for-the-badge&logo=dbt&logoColor=white)
![python](https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white)
![jmp/jsl](https://img.shields.io/badge/JMP-0076B6?style=for-the-badge&logo=JMP&logoColor=white)
![github-actions](https://img.shields.io/badge/GitHub_Actions-2088FF?style=for-the-badge&logo=github-actions&logoColor=white)
![sql](https://img.shields.io/badge/SQL-4479A1?style=for-the-badge&logo=sql&logoColor=white)

## Previous Work
### Lumentum Inc. (2022-Present)
#### Data Scientist

* Using Pytorch to classify chip defects in a production environment. 
* Designing global MLOps strategy/tooling/template for the company
* created a self-healing code bot using OpenAI to make suggestions on CI failures in the pull request. Reducing the time to fix
* Cross Functionally Championing the use of DBT and Databricks to create a more robust and scalable data pipeline.
* Created our dataops pipeline using dbt-databricks/github actions, including data quality checks and data lineage.  Lowering our deployment time from a week to a few hours.  
* Standardizing and templating our data models for high reuse to reduce the time to create new models from weeks to days.
* Lead the team in adopting a whole slew of new tools and processes:
    * Better version control: box -> git
    * Better data engineering: Excel/Tableau -> DBT/Databricks/Athena
    * Better orchestration: manual/windows task scheduler -> Jenkins/Databricks
    * Better code/data quality: manual -> GHActions/DBT/Pre-commit/linting
    * Better alerting/monitoring: None -> Teams/Email/Artifact Reporting
* Contributing to the open source projects that I love, just trying to give back to the community that has helped me so much.
* Optimizing our data models to reduce the time to run. (Best was 11 hours to ~5 minutes changing from pandas to pyspark)

### Predictum Inc. (2016-2022)
#### Chief Software Engineer
* Responsible for supporting all the company’s jsl and python developers with infrastructure and libraries to ensure they have a great coding experience
* Created a JMP [Self-Validated Ensemble Modeling](https://www.sciencedirect.com/science/article/abs/pii/S0169743921002070) (SVEM) addin for ML projects where runs are too expensive to add additional runs for validation. Based on Gen Reg
    * Since we created this, JMP has now implemented it [into their core product](https://www.jmp.com/support/help/en/17.2/index.shtml#page/jmp/overview-of-selfvalidated-ensemble-models.shtml)
* Oversee the design of an automated testing framework for all projects, focusing on shift left principles. 
    * Multiple versions on multiple OS’s using docker
    * Created a GitLab pipeline for JSL from scratch to test/encrypt/deploy
    * This completely changed the way that Predictum develops.  
* Created [an open source JSL extension for VSCode](https://gitlab.com/predictum/jsl-for-vscode) so users can more easily have access to standard IDE features
* Brought our first non-custom solution to market to help users share knowledge more easily

#### Lead Development Engineer
* Technical Expert on front end, database, modeling
* Cradle to grave product design
* Created new coding standard for new hires
* Trained customers in Modeling, UI, and statistics, and coding

#### Senior Development Engineer
* Architect Production Capable JMP/JSL/Python applications
* Automation of data analyses, LIMS, data entry/extraction/ETL
* Assisted Model Creation/DOE apps with the goal of lessening the statistical burden of the end user
* One button SPC dashboards with dynamic data selection/cleaning/filtering
* Industries include: wafer fab, biomedical, chemical, geology, consumer products, solar
* Implementing Six Sigma strategies to fully scope projects, to reducing scope creep and cutting time over estimate by 40%.
* Created dynamic workflow app that allows power users to create simplified statistically valid apps
* Created Custom Data Visualization methods, enhancing native capabilities
* Created localization standard and libraries for company-wide framework
* Database Design and Engineering using SQL Server, Oracle, HIVE, MySql, NoSql

### k-Space Associates Inc. (2015-2016)
* Develop new metrology methods and algorithms, primarily focused on optical non-contact metrologies for semiconductor manufacturing
* Developed a statistical method to quantify in-situ surface debris from images post clean to ensure proper cleaning
* Model light propagation through various materials to test validity of metrology paths
* Own database schema design and integration into software
* Created training and validation models for wafer measurements
* Created a python based thin-film optical simulator to test theoretical films

### First Solar Inc. (2011-2015)
#### Metrology Development Engineer
* Design of Experiments to aid in efficient development of higher efficiency panel recipes, getting high quality data with minimal downtime
* Leading equipment upgrade to allow for new chemistry in a fleet-wide wet chemistry tool
* Developed Reflectometry metrologies to measure in-situ CVD Deposition Chamber thickness and Anti Reflective Coating Efficiency resulting in a 2% Efficiency increase
* Developed new algorithm for the calculation of absorption in Band Gap Region to aid in film deposition control
* Reduced wasted space/time in space-constrained chambers by standardizing the method for selection of reliability modules and making the data more statistically significant
* Reduced line variation and improved module stabilized efficiency through analysis of new metrologies
    * Correlated outputs to any controllable parameter
* Enhanced scripting capabilities by building of a new library of JMP (JSL) functions
* Created JSL / Python Scripts for all of Development Engineering

#### Wet Processing Development Engineering Tech
* Tested various Electro-chemical Depositions of new metal layer for improvement of back contact
* Executed Design Of Experiment testing and improved defect necessary to roll out a new recipe
* Reduced scrap rate by an order of magnitude with DOE testing
* Improved module throughput by 10% by running on-line tests for New Recipe implementation
* Characterized the relationship between dopants in the film with reliability and efficiency gains 
* Provided original JMP and SQL scripts for the Wet Processing Group

## Education
### B.S. in Chemical Engineering from University of Toledo (2004-2009)

## Certifications
* Six Sigma Green Belt (2014)
* Databricks Certified Associate Developer for Apache Spark 3.0 (2022)
* STIPS/JSL/DOE JMP Certifications (2017)
